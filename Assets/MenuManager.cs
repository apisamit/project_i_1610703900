﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void MenuGame()
    {
        SceneManager.LoadScene("Menu");
    }
    public void StartGame ()
    {
        SceneManager.LoadScene("Game");
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void EndGame()
    {
        SceneManager.LoadScene("EndGame");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}