﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGun : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip bossFireSound;
    [SerializeField] float bossFireSoundVolume = 0.3f;

    public GameObject EnemyBullet;
    public float fireRate = 10f;

    private float nextFireTime;

    void Start()
    {

    }

    void Update()
    {
        if (nextFireTime < Time.time)
        {
            Invoke("FireEnemyBullet", 1f);
            nextFireTime = Time.time + fireRate;
        }
    }

    void FireEnemyBullet()
    {
        GameObject PlayerShip = GameObject.Find("PlayerShip");
        if(PlayerShip != null)
        {
            GameObject bullet = (GameObject)Instantiate(EnemyBullet);
            bullet.transform.position = transform.position;
            Vector2 direction = PlayerShip.transform.position - bullet.transform.position;
            bullet.GetComponent<EnemyBullet>().SetDirection(direction);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyFire);
            
        }
    }
}
