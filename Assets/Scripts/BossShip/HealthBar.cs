﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{
    private Image HP;
    public float CurrentHealth;
    private float MaxHealth = 1000;


    private void Start()
    {
        HP = GetComponent<Image>();

    }

    private void Update()
    {
        CurrentHealth = BossShip.Health;
        HP.fillAmount = CurrentHealth / MaxHealth;
    }
}
