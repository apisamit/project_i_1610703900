﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossShip : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip bossDied;
    [SerializeField] float bossDiedSoundVolume = 0.3f;

    public float speed;
    public static float Health;
    public bool MoveRight;
    private float Damage = 10;

    // Start is called before the first frame update
    void Start()
    {
        Health = 1000f;
    }

    // Update is called once per frame
    void Update()
    {
        if(MoveRight)
        {
            transform.Translate(2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(0.5f, 0.5f);
            Debug.Log("Left");
        }
        else
        {
            transform.Translate(-2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(-0.5f, 0.5f);
            Debug.Log("Right");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerShipTag") || (collision.tag == "PlayerBulletTag"))
        {
            Health -= Damage;
            Debug.Log("hit");

            if (Health <= 0)
            {
                AudioSource.PlayClipAtPoint(bossDied, Camera.main.transform.position, bossDiedSoundVolume);
                Destroy(gameObject);
                SceneManager.LoadScene("EndGame");
            }
            
        }

        if(collision.tag =="Turn")
        {
            if(MoveRight)
            {
                MoveRight = false;
            }
            else
            {
                MoveRight = true;
            }
        }
        
    }
}
