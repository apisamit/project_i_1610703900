﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static int spaceScore;

    [SerializeField] private GameObject enemySpawn;
    [SerializeField] private GameObject healthBarBoss;
    [SerializeField] private GameObject boss;

    // Start is called before the first frame update
    void Start()
    {
        spaceScore = 0;
        SoundManager.Instance.PlayBGM();
    }

    private void FixedUpdate()
    {
        SetActive();
    }

    public void SetActive()
    {
        if (spaceScore >= 10)
        {
            healthBarBoss.SetActive(true);
            boss.SetActive(true);
        }
    }
}
