﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    private Image HP;
    public float CurrentHealth;
    private float MaxHealth = 200;
    PlayerController PlayerShip;

    void Start()
    {
        HP = GetComponent<Image>();
        PlayerShip = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        CurrentHealth = PlayerShip.Health;
        HP.fillAmount = CurrentHealth / MaxHealth;
    }
}
