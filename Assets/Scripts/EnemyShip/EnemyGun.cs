﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;

public class EnemyGun : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip EnemyFireSound;
    [SerializeField] float EnemyFireSoundVolume = 0.3f;

    public GameObject EnemyBullet;

    void Start()
    {
        Invoke("FireEnemyBullet", 1f);
    }

    void Update()
    {

    }

    void FireEnemyBullet()
    {
        GameObject PlayerShip = GameObject.Find("PlayerShip");
        if (PlayerShip != null)
        {
            GameObject bullet = (GameObject)Instantiate(EnemyBullet);
            bullet.transform.position = transform.position;
            Vector2 direction = PlayerShip.transform.position - bullet.transform.position;
            bullet.GetComponent<EnemyBullet>().SetDirection(direction);

            SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyFire);
        }
    }
}
